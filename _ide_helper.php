<?php

namespace App\Facades {

    use App\Services\Form\Builder;
    use Closure;

    /**
     *
     */
    class FormBuilder
    {

        /**
         * Create a new form.
         * @static
         * @param array $params
         * @param Closure $callback
         * @return Builder
         */
        public static function create(array $params, Closure $callback) : Builder
        {
            /** @var Builder $instance */
            return $instance->create($params, $callback);
        }

    }

}

namespace {

    use Illuminate\Contracts\Pagination\LengthAwarePaginator;
    use Illuminate\Contracts\Pagination\Paginator;
    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Database\Query\Builder as QueryBuilder;
    use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Scope;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Database\Query\Grammars\Grammar;
    use Illuminate\Database\Query\Processors\Processor;

    class Eloquent extends Model
    {
        /**
         * Create and return an un-saved model instance.
         *
         * @param array $attributes
         * @return Model
         */
        public static function make($attributes = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->make($attributes);
        }

        /**
         * Register a new global scope.
         *
         * @param string $identifier
         * @param Scope|Closure $scope
         * @return EloquentBuilder
         */
        public static function withGlobalScope($identifier, $scope)
        {
            /** @var EloquentBuilder $instance */
            return $instance->withGlobalScope($identifier, $scope);
        }

        /**
         * Remove a registered global scope.
         *
         * @param Scope|string $scope
         * @return EloquentBuilder
         */
        public static function withoutGlobalScope($scope)
        {
            /** @var EloquentBuilder $instance */
            return $instance->withoutGlobalScope($scope);
        }

        /**
         * Remove all or passed registered global scopes.
         *
         * @param array|null $scopes
         * @return EloquentBuilder
         */
        public static function withoutGlobalScopes($scopes = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->withoutGlobalScopes($scopes);
        }

        /**
         * Get an array of global scopes that were removed from the query.
         *
         * @return array
         */
        public static function removedScopes()
        {
            /** @var EloquentBuilder $instance */
            return $instance->removedScopes();
        }

        /**
         * Add a where clause on the primary key to the query.
         *
         * @param mixed $id
         * @return EloquentBuilder
         */
        public static function whereKey($id)
        {
            /** @var EloquentBuilder $instance */
            return $instance->whereKey($id);
        }

        /**
         * Add a where clause on the primary key to the query.
         *
         * @param mixed $id
         * @return EloquentBuilder
         */
        public static function whereKeyNot($id)
        {
            /** @var EloquentBuilder $instance */
            return $instance->whereKeyNot($id);
        }

        /**
         * Add a basic where clause to the query.
         *
         * @param string|array|Closure $column
         * @param mixed $operator
         * @param mixed $value
         * @param string $boolean
         * @return EloquentBuilder
         */
        public static function where($column, $operator = null, $value = null, $boolean = 'and')
        {
            /** @var EloquentBuilder $instance */
            return $instance->where($column, $operator, $value, $boolean);
        }

        /**
         * Add an "or where" clause to the query.
         *
         * @param Closure|array|string $column
         * @param mixed $operator
         * @param mixed $value
         * @return EloquentBuilder|static
         */
        public static function orWhere($column, $operator = null, $value = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->orWhere($column, $operator, $value);
        }

        /**
         * Add an "order by" clause for a timestamp to the query.
         *
         * @param string $column
         * @return EloquentBuilder
         */
        public static function latest($column = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->latest($column);
        }

        /**
         * Add an "order by" clause for a timestamp to the query.
         *
         * @param string $column
         * @return EloquentBuilder
         */
        public static function oldest($column = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->oldest($column);
        }

        /**
         * Create a collection of models from plain arrays.
         *
         * @param array $items
         * @return Collection
         */
        public static function hydrate($items)
        {
            /** @var EloquentBuilder $instance */
            return $instance->hydrate($items);
        }

        /**
         * Create a collection of models from a raw query.
         *
         * @param string $query
         * @param array $bindings
         * @return Collection
         */
        public static function fromQuery($query, $bindings = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->fromQuery($query, $bindings);
        }

        /**
         * Find a model by its primary key.
         *
         * @param mixed $id
         * @param array $columns
         * @return Model|Collection|static[]|static|null
         */
        public static function find($id, $columns = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->find($id, $columns);
        }

        /**
         * Find multiple models by their primary keys.
         *
         * @param Arrayable|array $ids
         * @param array $columns
         * @return Collection
         */
        public static function findMany($ids, $columns = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->findMany($ids, $columns);
        }

        /**
         * Find a model by its primary key or throw an exception.
         *
         * @param mixed $id
         * @param array $columns
         * @return Model|Collection|static|static[]
         * @throws ModelNotFoundException
         */
        public static function findOrFail($id, $columns = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->findOrFail($id, $columns);
        }

        /**
         * Find a model by its primary key or return fresh model instance.
         *
         * @param mixed $id
         * @param array $columns
         * @return Model|static
         */
        public static function findOrNew($id, $columns = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->findOrNew($id, $columns);
        }

        /**
         * Get the first record matching the attributes or instantiate it.
         *
         * @param array $attributes
         * @param array $values
         * @return Model|static
         */
        public static function firstOrNew($attributes, $values = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->firstOrNew($attributes, $values);
        }

        /**
         * Get the first record matching the attributes or create it.
         *
         * @param array $attributes
         * @param array $values
         * @return Model|static
         */
        public static function firstOrCreate($attributes, $values = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->firstOrCreate($attributes, $values);
        }

        /**
         * Create or update a record matching the attributes, and fill it with values.
         *
         * @param array $attributes
         * @param array $values
         * @return Model|static
         */
        public static function updateOrCreate($attributes, $values = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->updateOrCreate($attributes, $values);
        }

        /**
         * Execute the query and get the first result or throw an exception.
         *
         * @param array $columns
         * @return Model|static
         * @throws ModelNotFoundException
         */
        public static function firstOrFail($columns = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->firstOrFail($columns);
        }

        /**
         * Execute the query and get the first result or call a callback.
         *
         * @param Closure|array $columns
         * @param Closure|null $callback
         * @return Model|static|mixed
         */
        public static function firstOr($columns = array(), $callback = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->firstOr($columns, $callback);
        }

        /**
         * Get a single column's value from the first result of a query.
         *
         * @param string $column
         * @return mixed
         */
        public static function value($column)
        {
            /** @var EloquentBuilder $instance */
            return $instance->value($column);
        }

        /**
         * Execute the query as a "select" statement.
         *
         * @param array $columns
         * @return Collection|static[]
         */
        public static function get($columns = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->get($columns);
        }

        /**
         * Get the hydrated models without eager loading.
         *
         * @param array $columns
         * @return Model[]|static[]
         */
        public static function getModels($columns = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->getModels($columns);
        }

        /**
         * Eager load the relationships for the models.
         *
         * @param array $models
         * @return array
         */
        public static function eagerLoadRelations($models)
        {
            /** @var EloquentBuilder $instance */
            return $instance->eagerLoadRelations($models);
        }

        /**
         * Get a generator for the given query.
         *
         * @return Generator
         */
        public static function cursor()
        {
            /** @var EloquentBuilder $instance */
            yield $instance->cursor();
        }

        /**
         * Chunk the results of a query by comparing numeric IDs.
         *
         * @param int $count
         * @param callable $callback
         * @param string|null $column
         * @param string|null $alias
         * @return bool
         */
        public static function chunkById($count, $callback, $column = null, $alias = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->chunkById($count, $callback, $column, $alias);
        }

        /**
         * Get an array with the values of a given column.
         *
         * @param string $column
         * @param string|null $key
         * @return \Illuminate\Support\Collection
         */
        public static function pluck($column, $key = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->pluck($column, $key);
        }

        /**
         * Paginate the given query.
         *
         * @param int $perPage
         * @param array $columns
         * @param string $pageName
         * @param int|null $page
         * @return LengthAwarePaginator
         * @throws InvalidArgumentException
         */
        public static function paginate($perPage = null, $columns = array(), $pageName = 'page', $page = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->paginate($perPage, $columns, $pageName, $page);
        }

        /**
         * Paginate the given query into a simple paginator.
         *
         * @param int $perPage
         * @param array $columns
         * @param string $pageName
         * @param int|null $page
         * @return Paginator
         */
        public static function simplePaginate($perPage = null, $columns = array(), $pageName = 'page', $page = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->simplePaginate($perPage, $columns, $pageName, $page);
        }

        /**
         * Save a new model and return the instance.
         *
         * @param array $attributes
         * @return Model|$this
         */
        public static function create($attributes = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->create($attributes);
        }

        /**
         * Save a new model and return the instance. Allow mass-assignment.
         *
         * @param array $attributes
         * @return Model|$this
         */
        public static function forceCreate($attributes)
        {
            /** @var EloquentBuilder $instance */
            return $instance->forceCreate($attributes);
        }

        /**
         * Register a replacement for the default delete function.
         *
         * @param Closure $callback
         * @return void
         */
        public static function onDelete($callback)
        {
            /** @var EloquentBuilder $instance */
            $instance->onDelete($callback);
        }

        /**
         * Call the given local model scopes.
         *
         * @param array $scopes
         * @return static|mixed
         */
        public static function scopes($scopes)
        {
            /** @var EloquentBuilder $instance */
            return $instance->scopes($scopes);
        }

        /**
         * Apply the scopes to the Eloquent builder instance and return it.
         *
         * @return EloquentBuilder
         */
        public static function applyScopes()
        {
            /** @var EloquentBuilder $instance */
            return $instance->applyScopes();
        }

        /**
         * Prevent the specified relations from being eager loaded.
         *
         * @param mixed $relations
         * @return EloquentBuilder
         */
        public static function without($relations)
        {
            /** @var EloquentBuilder $instance */
            return $instance->without($relations);
        }

        /**
         * Create a new instance of the model being queried.
         *
         * @param array $attributes
         * @return Model|static
         */
        public static function newModelInstance($attributes = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->newModelInstance($attributes);
        }

        /**
         * Get the underlying query builder instance.
         *
         * @return QueryBuilder
         */
        public static function getQuery()
        {
            /** @var EloquentBuilder $instance */
            return $instance->getQuery();
        }

        /**
         * Set the underlying query builder instance.
         *
         * @param QueryBuilder $query
         * @return EloquentBuilder
         */
        public static function setQuery($query)
        {
            /** @var EloquentBuilder $instance */
            return $instance->setQuery($query);
        }

        /**
         * Get a base query builder instance.
         *
         * @return QueryBuilder
         */
        public static function toBase()
        {
            /** @var EloquentBuilder $instance */
            return $instance->toBase();
        }

        /**
         * Get the relationships being eagerly loaded.
         *
         * @return array
         */
        public static function getEagerLoads()
        {
            /** @var EloquentBuilder $instance */
            return $instance->getEagerLoads();
        }

        /**
         * Set the relationships being eagerly loaded.
         *
         * @param array $eagerLoad
         * @return EloquentBuilder
         */
        public static function setEagerLoads($eagerLoad)
        {
            /** @var EloquentBuilder $instance */
            return $instance->setEagerLoads($eagerLoad);
        }

        /**
         * Get the model instance being queried.
         *
         * @return Model|static
         */
        public static function getModel()
        {
            /** @var EloquentBuilder $instance */
            return $instance->getModel();
        }

        /**
         * Set a model instance for the model being queried.
         *
         * @param Model $model
         * @return EloquentBuilder
         */
        public static function setModel($model)
        {
            /** @var EloquentBuilder $instance */
            return $instance->setModel($model);
        }

        /**
         * Get the given macro by name.
         *
         * @param string $name
         * @return Closure
         */
        public static function getMacro($name)
        {
            /** @var EloquentBuilder $instance */
            return $instance->getMacro($name);
        }

        /**
         * Chunk the results of the query.
         *
         * @param int $count
         * @param callable $callback
         * @return bool
         */
        public static function chunk($count, $callback)
        {
            /** @var EloquentBuilder $instance */
            return $instance->chunk($count, $callback);
        }

        /**
         * Execute a callback over each item while chunking.
         *
         * @param callable $callback
         * @param int $count
         * @return bool
         */
        public static function each($callback, $count = 1000)
        {
            /** @var EloquentBuilder $instance */
            return $instance->each($callback, $count);
        }

        /**
         * Execute the query and get the first result.
         *
         * @param array $columns
         * @return Model|object|static|null
         */
        public static function first($columns = array())
        {
            /** @var EloquentBuilder $instance */
            return $instance->first($columns);
        }

        /**
         * Apply the callback's query changes if the given "value" is true.
         *
         * @param mixed $value
         * @param callable $callback
         * @param callable|null $default
         * @return mixed|$this
         */
        public static function when($value, $callback, $default = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->when($value, $callback, $default);
        }

        /**
         * Pass the query to a given callback.
         *
         * @param callable $callback
         * @return QueryBuilder
         */
        public static function tap($callback)
        {
            /** @var EloquentBuilder $instance */
            return $instance->tap($callback);
        }

        /**
         * Apply the callback's query changes if the given "value" is false.
         *
         * @param mixed $value
         * @param callable $callback
         * @param callable|null $default
         * @return mixed|$this
         */
        public static function unless($value, $callback, $default = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->unless($value, $callback, $default);
        }

        /**
         * Add a relationship count / exists condition to the query.
         *
         * @param string $relation
         * @param string $operator
         * @param int $count
         * @param string $boolean
         * @param Closure|null $callback
         * @return EloquentBuilder|static
         */
        public static function has($relation, $operator = '>=', $count = 1, $boolean = 'and', $callback = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->has($relation, $operator, $count, $boolean, $callback);
        }

        /**
         * Add a relationship count / exists condition to the query with an "or".
         *
         * @param string $relation
         * @param string $operator
         * @param int $count
         * @return EloquentBuilder|static
         */
        public static function orHas($relation, $operator = '>=', $count = 1)
        {
            /** @var EloquentBuilder $instance */
            return $instance->orHas($relation, $operator, $count);
        }

        /**
         * Add a relationship count / exists condition to the query.
         *
         * @param string $relation
         * @param string $boolean
         * @param Closure|null $callback
         * @return EloquentBuilder|static
         */
        public static function doesntHave($relation, $boolean = 'and', $callback = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->doesntHave($relation, $boolean, $callback);
        }

        /**
         * Add a relationship count / exists condition to the query with an "or".
         *
         * @param string $relation
         * @return EloquentBuilder|static
         */
        public static function orDoesntHave($relation)
        {
            /** @var EloquentBuilder $instance */
            return $instance->orDoesntHave($relation);
        }

        /**
         * Add a relationship count / exists condition to the query with where clauses.
         *
         * @param string $relation
         * @param Closure|null $callback
         * @param string $operator
         * @param int $count
         * @return EloquentBuilder|static
         */
        public static function whereHas($relation, $callback = null, $operator = '>=', $count = 1)
        {
            /** @var EloquentBuilder $instance */
            return $instance->whereHas($relation, $callback, $operator, $count);
        }

        /**
         * Add a relationship count / exists condition to the query with where clauses and an "or".
         *
         * @param string $relation
         * @param Closure $callback
         * @param string $operator
         * @param int $count
         * @return EloquentBuilder|static
         */
        public static function orWhereHas($relation, $callback = null, $operator = '>=', $count = 1)
        {
            /** @var EloquentBuilder $instance */
            return $instance->orWhereHas($relation, $callback, $operator, $count);
        }

        /**
         * Add a relationship count / exists condition to the query with where clauses.
         *
         * @param string $relation
         * @param Closure|null $callback
         * @return EloquentBuilder|static
         */
        public static function whereDoesntHave($relation, $callback = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->whereDoesntHave($relation, $callback);
        }

        /**
         * Add a relationship count / exists condition to the query with where clauses and an "or".
         *
         * @param string $relation
         * @param Closure $callback
         * @return EloquentBuilder|static
         */
        public static function orWhereDoesntHave($relation, $callback = null)
        {
            /** @var EloquentBuilder $instance */
            return $instance->orWhereDoesntHave($relation, $callback);
        }

        /**
         * Add subselect queries to count the relations.
         *
         * @param mixed $relations
         * @return EloquentBuilder
         */
        public static function withCount($relations)
        {
            /** @var EloquentBuilder $instance */
            return $instance->withCount($relations);
        }

        /**
         * Merge the where constraints from another query to the current query.
         *
         * @param EloquentBuilder $from
         * @return EloquentBuilder|static
         */
        public static function mergeConstraintsFrom($from)
        {
            /** @var EloquentBuilder $instance */
            return $instance->mergeConstraintsFrom($from);
        }

        /**
         * Set the columns to be selected.
         *
         * @param array|mixed $columns
         * @return QueryBuilder
         */
        public static function select($columns = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->select($columns);
        }

        /**
         * Add a subselect expression to the query.
         *
         * @param Closure|QueryBuilder|string $query
         * @param string $as
         * @return QueryBuilder|static
         * @throws InvalidArgumentException
         */
        public static function selectSub($query, $as)
        {
            /** @var QueryBuilder $instance */
            return $instance->selectSub($query, $as);
        }

        /**
         * Add a new "raw" select expression to the query.
         *
         * @param string $expression
         * @param array $bindings
         * @return QueryBuilder|static
         */
        public static function selectRaw($expression, $bindings = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->selectRaw($expression, $bindings);
        }

        /**
         * Makes "from" fetch from a subquery.
         *
         * @param Closure|QueryBuilder|string $query
         * @param string $as
         * @return QueryBuilder|static
         * @throws InvalidArgumentException
         */
        public static function fromSub($query, $as)
        {
            /** @var QueryBuilder $instance */
            return $instance->fromSub($query, $as);
        }

        /**
         * Add a raw from clause to the query.
         *
         * @param string $expression
         * @param mixed $bindings
         * @return QueryBuilder|static
         */
        public static function fromRaw($expression, $bindings = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->fromRaw($expression, $bindings);
        }

        /**
         * Add a new select column to the query.
         *
         * @param array|mixed $column
         * @return QueryBuilder
         */
        public static function addSelect($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->addSelect($column);
        }

        /**
         * Force the query to only return distinct results.
         *
         * @return QueryBuilder
         */
        public static function distinct()
        {
            /** @var QueryBuilder $instance */
            return $instance->distinct();
        }

        /**
         * Set the table which the query is targeting.
         *
         * @param string $table
         * @return QueryBuilder
         */
        public static function from($table)
        {
            /** @var QueryBuilder $instance */
            return $instance->from($table);
        }

        /**
         * Add a join clause to the query.
         *
         * @param string $table
         * @param Closure|string $first
         * @param string|null $operator
         * @param string|null $second
         * @param string $type
         * @param bool $where
         * @return QueryBuilder
         */
        public static function join($table, $first, $operator = null, $second = null, $type = 'inner', $where = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->join($table, $first, $operator, $second, $type, $where);
        }

        /**
         * Add a "join where" clause to the query.
         *
         * @param string $table
         * @param Closure|string $first
         * @param string $operator
         * @param string $second
         * @param string $type
         * @return QueryBuilder|static
         */
        public static function joinWhere($table, $first, $operator, $second, $type = 'inner')
        {
            /** @var QueryBuilder $instance */
            return $instance->joinWhere($table, $first, $operator, $second, $type);
        }

        /**
         * Add a subquery join clause to the query.
         *
         * @param Closure|QueryBuilder|string $query
         * @param string $as
         * @param Closure|string $first
         * @param string|null $operator
         * @param string|null $second
         * @param string $type
         * @param bool $where
         * @return QueryBuilder|static
         * @throws InvalidArgumentException
         */
        public static function joinSub($query, $as, $first, $operator = null, $second = null, $type = 'inner', $where = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->joinSub($query, $as, $first, $operator, $second, $type, $where);
        }

        /**
         * Add a left join to the query.
         *
         * @param string $table
         * @param Closure|string $first
         * @param string|null $operator
         * @param string|null $second
         * @return QueryBuilder|static
         */
        public static function leftJoin($table, $first, $operator = null, $second = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->leftJoin($table, $first, $operator, $second);
        }

        /**
         * Add a "join where" clause to the query.
         *
         * @param string $table
         * @param Closure|string $first
         * @param string $operator
         * @param string $second
         * @return QueryBuilder|static
         */
        public static function leftJoinWhere($table, $first, $operator, $second)
        {
            /** @var QueryBuilder $instance */
            return $instance->leftJoinWhere($table, $first, $operator, $second);
        }

        /**
         * Add a subquery left join to the query.
         *
         * @param Closure|QueryBuilder|string $query
         * @param string $as
         * @param Closure|string $first
         * @param string|null $operator
         * @param string|null $second
         * @return QueryBuilder|static
         */
        public static function leftJoinSub($query, $as, $first, $operator = null, $second = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->leftJoinSub($query, $as, $first, $operator, $second);
        }

        /**
         * Add a right join to the query.
         *
         * @param string $table
         * @param Closure|string $first
         * @param string|null $operator
         * @param string|null $second
         * @return QueryBuilder|static
         */
        public static function rightJoin($table, $first, $operator = null, $second = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->rightJoin($table, $first, $operator, $second);
        }

        /**
         * Add a "right join where" clause to the query.
         *
         * @param string $table
         * @param Closure|string $first
         * @param string $operator
         * @param string $second
         * @return QueryBuilder|static
         */
        public static function rightJoinWhere($table, $first, $operator, $second)
        {
            /** @var QueryBuilder $instance */
            return $instance->rightJoinWhere($table, $first, $operator, $second);
        }

        /**
         * Add a subquery right join to the query.
         *
         * @param Closure|QueryBuilder|string $query
         * @param string $as
         * @param Closure|string $first
         * @param string|null $operator
         * @param string|null $second
         * @return QueryBuilder|static
         */
        public static function rightJoinSub($query, $as, $first, $operator = null, $second = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->rightJoinSub($query, $as, $first, $operator, $second);
        }

        /**
         * Add a "cross join" clause to the query.
         *
         * @param string $table
         * @param Closure|string|null $first
         * @param string|null $operator
         * @param string|null $second
         * @return QueryBuilder|static
         */
        public static function crossJoin($table, $first = null, $operator = null, $second = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->crossJoin($table, $first, $operator, $second);
        }

        /**
         * Merge an array of where clauses and bindings.
         *
         * @param array $wheres
         * @param array $bindings
         * @return void
         */
        public static function mergeWheres($wheres, $bindings)
        {
            /** @var QueryBuilder $instance */
            $instance->mergeWheres($wheres, $bindings);
        }

        /**
         * Prepare the value and operator for a where clause.
         *
         * @param string $value
         * @param string $operator
         * @param bool $useDefault
         * @return array
         * @throws InvalidArgumentException
         */
        public static function prepareValueAndOperator($value, $operator, $useDefault = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->prepareValueAndOperator($value, $operator, $useDefault);
        }

        /**
         * Add a "where" clause comparing two columns to the query.
         *
         * @param string|array $first
         * @param string|null $operator
         * @param string|null $second
         * @param string|null $boolean
         * @return QueryBuilder|static
         */
        public static function whereColumn($first, $operator = null, $second = null, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereColumn($first, $operator, $second, $boolean);
        }

        /**
         * Add an "or where" clause comparing two columns to the query.
         *
         * @param string|array $first
         * @param string|null $operator
         * @param string|null $second
         * @return QueryBuilder|static
         */
        public static function orWhereColumn($first, $operator = null, $second = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereColumn($first, $operator, $second);
        }

        /**
         * Add a raw where clause to the query.
         *
         * @param string $sql
         * @param mixed $bindings
         * @param string $boolean
         * @return QueryBuilder
         */
        public static function whereRaw($sql, $bindings = array(), $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereRaw($sql, $bindings, $boolean);
        }

        /**
         * Add a raw or where clause to the query.
         *
         * @param string $sql
         * @param mixed $bindings
         * @return QueryBuilder|static
         */
        public static function orWhereRaw($sql, $bindings = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereRaw($sql, $bindings);
        }

        /**
         * Add a "where in" clause to the query.
         *
         * @param string $column
         * @param mixed $values
         * @param string $boolean
         * @param bool $not
         * @return QueryBuilder
         */
        public static function whereIn($column, $values, $boolean = 'and', $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->whereIn($column, $values, $boolean, $not);
        }

        /**
         * Add an "or where in" clause to the query.
         *
         * @param string $column
         * @param mixed $values
         * @return QueryBuilder|static
         */
        public static function orWhereIn($column, $values)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereIn($column, $values);
        }

        /**
         * Add a "where not in" clause to the query.
         *
         * @param string $column
         * @param mixed $values
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereNotIn($column, $values, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereNotIn($column, $values, $boolean);
        }

        /**
         * Add an "or where not in" clause to the query.
         *
         * @param string $column
         * @param mixed $values
         * @return QueryBuilder|static
         */
        public static function orWhereNotIn($column, $values)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereNotIn($column, $values);
        }

        /**
         * Add a "where in raw" clause for integer values to the query.
         *
         * @param string $column
         * @param Arrayable|array $values
         * @param string $boolean
         * @param bool $not
         * @return QueryBuilder
         */
        public static function whereIntegerInRaw($column, $values, $boolean = 'and', $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->whereIntegerInRaw($column, $values, $boolean, $not);
        }

        /**
         * Add a "where not in raw" clause for integer values to the query.
         *
         * @param string $column
         * @param Arrayable|array $values
         * @param string $boolean
         * @return QueryBuilder
         */
        public static function whereIntegerNotInRaw($column, $values, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereIntegerNotInRaw($column, $values, $boolean);
        }

        /**
         * Add a "where null" clause to the query.
         *
         * @param string $column
         * @param string $boolean
         * @param bool $not
         * @return QueryBuilder
         */
        public static function whereNull($column, $boolean = 'and', $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->whereNull($column, $boolean, $not);
        }

        /**
         * Add an "or where null" clause to the query.
         *
         * @param string $column
         * @return QueryBuilder|static
         */
        public static function orWhereNull($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereNull($column);
        }

        /**
         * Add a "where not null" clause to the query.
         *
         * @param string $column
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereNotNull($column, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereNotNull($column, $boolean);
        }

        /**
         * Add a where between statement to the query.
         *
         * @param string $column
         * @param array $values
         * @param string $boolean
         * @param bool $not
         * @return QueryBuilder
         */
        public static function whereBetween($column, $values, $boolean = 'and', $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->whereBetween($column, $values, $boolean, $not);
        }

        /**
         * Add an or where between statement to the query.
         *
         * @param string $column
         * @param array $values
         * @return QueryBuilder|static
         */
        public static function orWhereBetween($column, $values)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereBetween($column, $values);
        }

        /**
         * Add a where not between statement to the query.
         *
         * @param string $column
         * @param array $values
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereNotBetween($column, $values, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereNotBetween($column, $values, $boolean);
        }

        /**
         * Add an or where not between statement to the query.
         *
         * @param string $column
         * @param array $values
         * @return QueryBuilder|static
         */
        public static function orWhereNotBetween($column, $values)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereNotBetween($column, $values);
        }

        /**
         * Add an "or where not null" clause to the query.
         *
         * @param string $column
         * @return QueryBuilder|static
         */
        public static function orWhereNotNull($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereNotNull($column);
        }

        /**
         * Add a "where date" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|null $value
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereDate($column, $operator, $value = null, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereDate($column, $operator, $value, $boolean);
        }

        /**
         * Add an "or where date" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|null $value
         * @return QueryBuilder|static
         */
        public static function orWhereDate($column, $operator, $value = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereDate($column, $operator, $value);
        }

        /**
         * Add a "where time" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|null $value
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereTime($column, $operator, $value = null, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereTime($column, $operator, $value, $boolean);
        }

        /**
         * Add an "or where time" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|null $value
         * @return QueryBuilder|static
         */
        public static function orWhereTime($column, $operator, $value = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereTime($column, $operator, $value);
        }

        /**
         * Add a "where day" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|null $value
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereDay($column, $operator, $value = null, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereDay($column, $operator, $value, $boolean);
        }

        /**
         * Add an "or where day" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|null $value
         * @return QueryBuilder|static
         */
        public static function orWhereDay($column, $operator, $value = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereDay($column, $operator, $value);
        }

        /**
         * Add a "where month" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|null $value
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereMonth($column, $operator, $value = null, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereMonth($column, $operator, $value, $boolean);
        }

        /**
         * Add an "or where month" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|null $value
         * @return QueryBuilder|static
         */
        public static function orWhereMonth($column, $operator, $value = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereMonth($column, $operator, $value);
        }

        /**
         * Add a "where year" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|int|null $value
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereYear($column, $operator, $value = null, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereYear($column, $operator, $value, $boolean);
        }

        /**
         * Add an "or where year" statement to the query.
         *
         * @param string $column
         * @param string $operator
         * @param DateTimeInterface|string|int|null $value
         * @return QueryBuilder|static
         */
        public static function orWhereYear($column, $operator, $value = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereYear($column, $operator, $value);
        }

        /**
         * Add a nested where statement to the query.
         *
         * @param Closure $callback
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereNested($callback, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereNested($callback, $boolean);
        }

        /**
         * Create a new query instance for nested where condition.
         *
         * @return QueryBuilder
         */
        public static function forNestedWhere()
        {
            /** @var QueryBuilder $instance */
            return $instance->forNestedWhere();
        }

        /**
         * Add another query builder as a nested where to the query builder.
         *
         * @param QueryBuilder|static $query
         * @param string $boolean
         * @return QueryBuilder
         */
        public static function addNestedWhereQuery($query, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->addNestedWhereQuery($query, $boolean);
        }

        /**
         * Add an exists clause to the query.
         *
         * @param Closure $callback
         * @param string $boolean
         * @param bool $not
         * @return QueryBuilder
         */
        public static function whereExists($callback, $boolean = 'and', $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->whereExists($callback, $boolean, $not);
        }

        /**
         * Add an or exists clause to the query.
         *
         * @param Closure $callback
         * @param bool $not
         * @return QueryBuilder|static
         */
        public static function orWhereExists($callback, $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereExists($callback, $not);
        }

        /**
         * Add a where not exists clause to the query.
         *
         * @param Closure $callback
         * @param string $boolean
         * @return QueryBuilder|static
         */
        public static function whereNotExists($callback, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereNotExists($callback, $boolean);
        }

        /**
         * Add a where not exists clause to the query.
         *
         * @param Closure $callback
         * @return QueryBuilder|static
         */
        public static function orWhereNotExists($callback)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereNotExists($callback);
        }

        /**
         * Add an exists clause to the query.
         *
         * @param QueryBuilder $query
         * @param string $boolean
         * @param bool $not
         * @return QueryBuilder
         */
        public static function addWhereExistsQuery($query, $boolean = 'and', $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->addWhereExistsQuery($query, $boolean, $not);
        }

        /**
         * Adds a where condition using row values.
         *
         * @param array $columns
         * @param string $operator
         * @param array $values
         * @param string $boolean
         * @return QueryBuilder
         */
        public static function whereRowValues($columns, $operator, $values, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereRowValues($columns, $operator, $values, $boolean);
        }

        /**
         * Adds a or where condition using row values.
         *
         * @param array $columns
         * @param string $operator
         * @param array $values
         * @return QueryBuilder
         */
        public static function orWhereRowValues($columns, $operator, $values)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereRowValues($columns, $operator, $values);
        }

        /**
         * Add a "where JSON contains" clause to the query.
         *
         * @param string $column
         * @param mixed $value
         * @param string $boolean
         * @param bool $not
         * @return QueryBuilder
         */
        public static function whereJsonContains($column, $value, $boolean = 'and', $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->whereJsonContains($column, $value, $boolean, $not);
        }

        /**
         * Add a "or where JSON contains" clause to the query.
         *
         * @param string $column
         * @param mixed $value
         * @return QueryBuilder
         */
        public static function orWhereJsonContains($column, $value)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereJsonContains($column, $value);
        }

        /**
         * Add a "where JSON not contains" clause to the query.
         *
         * @param string $column
         * @param mixed $value
         * @param string $boolean
         * @return QueryBuilder
         */
        public static function whereJsonDoesntContain($column, $value, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereJsonDoesntContain($column, $value, $boolean);
        }

        /**
         * Add a "or where JSON not contains" clause to the query.
         *
         * @param string $column
         * @param mixed $value
         * @return QueryBuilder
         */
        public static function orWhereJsonDoesntContain($column, $value)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereJsonDoesntContain($column, $value);
        }

        /**
         * Add a "where JSON length" clause to the query.
         *
         * @param string $column
         * @param mixed $operator
         * @param mixed $value
         * @param string $boolean
         * @return QueryBuilder
         */
        public static function whereJsonLength($column, $operator, $value = null, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->whereJsonLength($column, $operator, $value, $boolean);
        }

        /**
         * Add a "or where JSON length" clause to the query.
         *
         * @param string $column
         * @param mixed $operator
         * @param mixed $value
         * @return QueryBuilder
         */
        public static function orWhereJsonLength($column, $operator, $value = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->orWhereJsonLength($column, $operator, $value);
        }

        /**
         * Handles dynamic "where" clauses to the query.
         *
         * @param string $method
         * @param array $parameters
         * @return QueryBuilder
         */
        public static function dynamicWhere($method, $parameters)
        {
            /** @var QueryBuilder $instance */
            return $instance->dynamicWhere($method, $parameters);
        }

        /**
         * Add a "group by" clause to the query.
         *
         * @param array $groups
         * @return QueryBuilder
         */
        public static function groupBy($groups = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->groupBy($groups);
        }

        /**
         * Add a "having" clause to the query.
         *
         * @param string $column
         * @param string|null $operator
         * @param string|null $value
         * @param string $boolean
         * @return QueryBuilder
         */
        public static function having($column, $operator = null, $value = null, $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->having($column, $operator, $value, $boolean);
        }

        /**
         * Add a "or having" clause to the query.
         *
         * @param string $column
         * @param string|null $operator
         * @param string|null $value
         * @return QueryBuilder|static
         */
        public static function orHaving($column, $operator = null, $value = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->orHaving($column, $operator, $value);
        }

        /**
         * Add a "having between " clause to the query.
         *
         * @param string $column
         * @param array $values
         * @param string $boolean
         * @param bool $not
         * @return QueryBuilder|static
         */
        public static function havingBetween($column, $values, $boolean = 'and', $not = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->havingBetween($column, $values, $boolean, $not);
        }

        /**
         * Add a raw having clause to the query.
         *
         * @param string $sql
         * @param array $bindings
         * @param string $boolean
         * @return QueryBuilder
         */
        public static function havingRaw($sql, $bindings = array(), $boolean = 'and')
        {
            /** @var QueryBuilder $instance */
            return $instance->havingRaw($sql, $bindings, $boolean);
        }

        /**
         * Add a raw or having clause to the query.
         *
         * @param string $sql
         * @param array $bindings
         * @return QueryBuilder|static
         */
        public static function orHavingRaw($sql, $bindings = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->orHavingRaw($sql, $bindings);
        }

        /**
         * Add an "order by" clause to the query.
         *
         * @param string $column
         * @param string $direction
         * @return QueryBuilder
         * @throws InvalidArgumentException
         */
        public static function orderBy($column, $direction = 'asc')
        {
            /** @var QueryBuilder $instance */
            return $instance->orderBy($column, $direction);
        }

        /**
         * Add a descending "order by" clause to the query.
         *
         * @param string $column
         * @return QueryBuilder
         */
        public static function orderByDesc($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->orderByDesc($column);
        }

        /**
         * Put the query's results in random order.
         *
         * @param string $seed
         * @return QueryBuilder
         */
        public static function inRandomOrder($seed = '')
        {
            /** @var QueryBuilder $instance */
            return $instance->inRandomOrder($seed);
        }

        /**
         * Add a raw "order by" clause to the query.
         *
         * @param string $sql
         * @param array $bindings
         * @return QueryBuilder
         */
        public static function orderByRaw($sql, $bindings = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->orderByRaw($sql, $bindings);
        }

        /**
         * Alias to set the "offset" value of the query.
         *
         * @param int $value
         * @return QueryBuilder|static
         */
        public static function skip($value)
        {
            /** @var QueryBuilder $instance */
            return $instance->skip($value);
        }

        /**
         * Set the "offset" value of the query.
         *
         * @param int $value
         * @return QueryBuilder
         */
        public static function offset($value)
        {
            /** @var QueryBuilder $instance */
            return $instance->offset($value);
        }

        /**
         * Alias to set the "limit" value of the query.
         *
         * @param int $value
         * @return QueryBuilder|static
         */
        public static function take($value)
        {
            /** @var QueryBuilder $instance */
            return $instance->take($value);
        }

        /**
         * Set the "limit" value of the query.
         *
         * @param int $value
         * @return QueryBuilder
         */
        public static function limit($value)
        {
            /** @var QueryBuilder $instance */
            return $instance->limit($value);
        }

        /**
         * Set the limit and offset for a given page.
         *
         * @param int $page
         * @param int $perPage
         * @return QueryBuilder|static
         */
        public static function forPage($page, $perPage = 15)
        {
            /** @var QueryBuilder $instance */
            return $instance->forPage($page, $perPage);
        }

        /**
         * Constrain the query to the previous "page" of results before a given ID.
         *
         * @param int $perPage
         * @param int|null $lastId
         * @param string $column
         * @return QueryBuilder|static
         */
        public static function forPageBeforeId($perPage = 15, $lastId = 0, $column = 'id')
        {
            /** @var QueryBuilder $instance */
            return $instance->forPageBeforeId($perPage, $lastId, $column);
        }

        /**
         * Constrain the query to the next "page" of results after a given ID.
         *
         * @param int $perPage
         * @param int|null $lastId
         * @param string $column
         * @return QueryBuilder|static
         */
        public static function forPageAfterId($perPage = 15, $lastId = 0, $column = 'id')
        {
            /** @var QueryBuilder $instance */
            return $instance->forPageAfterId($perPage, $lastId, $column);
        }

        /**
         * Add a union statement to the query.
         *
         * @param QueryBuilder|Closure $query
         * @param bool $all
         * @return QueryBuilder|static
         */
        public static function union($query, $all = false)
        {
            /** @var QueryBuilder $instance */
            return $instance->union($query, $all);
        }

        /**
         * Add a union all statement to the query.
         *
         * @param QueryBuilder|Closure $query
         * @return QueryBuilder|static
         */
        public static function unionAll($query)
        {
            /** @var QueryBuilder $instance */
            return $instance->unionAll($query);
        }

        /**
         * Lock the selected rows in the table.
         *
         * @param string|bool $value
         * @return QueryBuilder
         */
        public static function lock($value = true)
        {
            /** @var QueryBuilder $instance */
            return $instance->lock($value);
        }

        /**
         * Lock the selected rows in the table for updating.
         *
         * @return QueryBuilder
         */
        public static function lockForUpdate()
        {
            /** @var QueryBuilder $instance */
            return $instance->lockForUpdate();
        }

        /**
         * Share lock the selected rows in the table.
         *
         * @return QueryBuilder
         */
        public static function sharedLock()
        {
            /** @var QueryBuilder $instance */
            return $instance->sharedLock();
        }

        /**
         * Get the SQL representation of the query.
         *
         * @return string
         */
        public static function toSql()
        {
            /** @var QueryBuilder $instance */
            return $instance->toSql();
        }

        /**
         * Get the count of the total records for the paginator.
         *
         * @param array $columns
         * @return int
         */
        public static function getCountForPagination($columns = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->getCountForPagination($columns);
        }

        /**
         * Concatenate values of a given column as a string.
         *
         * @param string $column
         * @param string $glue
         * @return string
         */
        public static function implode($column, $glue = '')
        {
            /** @var QueryBuilder $instance */
            return $instance->implode($column, $glue);
        }

        /**
         * Determine if any rows exist for the current query.
         *
         * @return bool
         */
        public static function exists()
        {
            /** @var QueryBuilder $instance */
            return $instance->exists();
        }

        /**
         * Determine if no rows exist for the current query.
         *
         * @return bool
         */
        public static function doesntExist()
        {
            /** @var QueryBuilder $instance */
            return $instance->doesntExist();
        }

        /**
         * Retrieve the "count" result of the query.
         *
         * @param string $columns
         * @return int
         */
        public static function count($columns = '*')
        {
            /** @var QueryBuilder $instance */
            return $instance->count($columns);
        }

        /**
         * Retrieve the minimum value of a given column.
         *
         * @param string $column
         * @return mixed
         */
        public static function min($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->min($column);
        }

        /**
         * Retrieve the maximum value of a given column.
         *
         * @param string $column
         * @return mixed
         */
        public static function max($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->max($column);
        }

        /**
         * Retrieve the sum of the values of a given column.
         *
         * @param string $column
         * @return mixed
         */
        public static function sum($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->sum($column);
        }

        /**
         * Retrieve the average of the values of a given column.
         *
         * @param string $column
         * @return mixed
         */
        public static function avg($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->avg($column);
        }

        /**
         * Alias for the "avg" method.
         *
         * @param string $column
         * @return mixed
         */
        public static function average($column)
        {
            /** @var QueryBuilder $instance */
            return $instance->average($column);
        }

        /**
         * Execute an aggregate function on the database.
         *
         * @param string $function
         * @param array $columns
         * @return mixed
         */
        public static function aggregate($function, $columns = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->aggregate($function, $columns);
        }

        /**
         * Execute a numeric aggregate function on the database.
         *
         * @param string $function
         * @param array $columns
         * @return float|int
         */
        public static function numericAggregate($function, $columns = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->numericAggregate($function, $columns);
        }

        /**
         * Insert a new record into the database.
         *
         * @param array $values
         * @return bool
         */
        public static function insert($values)
        {
            /** @var QueryBuilder $instance */
            return $instance->insert($values);
        }

        /**
         * Insert a new record and get the value of the primary key.
         *
         * @param array $values
         * @param string|null $sequence
         * @return int
         */
        public static function insertGetId($values, $sequence = null)
        {
            /** @var QueryBuilder $instance */
            return $instance->insertGetId($values, $sequence);
        }

        /**
         * Insert new records into the table using a subquery.
         *
         * @param array $columns
         * @param Closure|QueryBuilder|string $query
         * @return bool
         */
        public static function insertUsing($columns, $query)
        {
            /** @var QueryBuilder $instance */
            return $instance->insertUsing($columns, $query);
        }

        /**
         * Insert or update a record matching the attributes, and fill it with values.
         *
         * @param array $attributes
         * @param array $values
         * @return bool
         */
        public static function updateOrInsert($attributes, $values = array())
        {
            /** @var QueryBuilder $instance */
            return $instance->updateOrInsert($attributes, $values);
        }

        /**
         * Run a truncate statement on the table.
         *
         * @return void
         */
        public static function truncate()
        {
            /** @var QueryBuilder $instance */
            $instance->truncate();
        }

        /**
         * Create a raw database expression.
         *
         * @param mixed $value
         * @return Expression
         */
        public static function raw($value)
        {
            /** @var QueryBuilder $instance */
            return $instance->raw($value);
        }

        /**
         * Get the current query value bindings in a flattened array.
         *
         * @return array
         */
        public static function getBindings()
        {
            /** @var QueryBuilder $instance */
            return $instance->getBindings();
        }

        /**
         * Get the raw array of bindings.
         *
         * @return array
         */
        public static function getRawBindings()
        {
            /** @var QueryBuilder $instance */
            return $instance->getRawBindings();
        }

        /**
         * Set the bindings on the query builder.
         *
         * @param array $bindings
         * @param string $type
         * @return QueryBuilder
         * @throws InvalidArgumentException
         */
        public static function setBindings($bindings, $type = 'where')
        {
            /** @var QueryBuilder $instance */
            return $instance->setBindings($bindings, $type);
        }

        /**
         * Add a binding to the query.
         *
         * @param mixed $value
         * @param string $type
         * @return QueryBuilder
         * @throws InvalidArgumentException
         */
        public static function addBinding($value, $type = 'where')
        {
            /** @var QueryBuilder $instance */
            return $instance->addBinding($value, $type);
        }

        /**
         * Merge an array of bindings into our bindings.
         *
         * @param QueryBuilder $query
         * @return QueryBuilder
         */
        public static function mergeBindings($query)
        {
            /** @var QueryBuilder $instance */
            return $instance->mergeBindings($query);
        }

        /**
         * Get the database query processor instance.
         *
         * @return Processor
         */
        public static function getProcessor()
        {
            /** @var QueryBuilder $instance */
            return $instance->getProcessor();
        }

        /**
         * Get the query grammar instance.
         *
         * @return Grammar
         */
        public static function getGrammar()
        {
            /** @var QueryBuilder $instance */
            return $instance->getGrammar();
        }

        /**
         * Use the write pdo for query.
         *
         * @return QueryBuilder
         */
        public static function useWritePdo()
        {
            /** @var QueryBuilder $instance */
            return $instance->useWritePdo();
        }

        /**
         * Clone the query without the given properties.
         *
         * @param array $properties
         * @return QueryBuilder
         */
        public static function cloneWithout($properties)
        {
            /** @var QueryBuilder $instance */
            return $instance->cloneWithout($properties);
        }

        /**
         * Clone the query without the given bindings.
         *
         * @param array $except
         * @return QueryBuilder
         */
        public static function cloneWithoutBindings($except)
        {
            /** @var QueryBuilder $instance */
            return $instance->cloneWithoutBindings($except);
        }

        /**
         * Dump the current SQL and bindings.
         *
         * @return void
         */
        public static function dump()
        {
            /** @var QueryBuilder $instance */
            $instance->dump();
        }

        /**
         * Die and dump the current SQL and bindings.
         *
         * @return void
         */
        public static function dd()
        {
            /** @var QueryBuilder $instance */
            $instance->dd();
        }

        /**
         * Register a custom macro.
         *
         * @param string $name
         * @param object|callable $macro
         * @return void
         */
        public static function macro($name, $macro)
        {
            QueryBuilder::macro($name, $macro);
        }

        /**
         * Mix another object into the class.
         *
         * @param object $mixin
         * @param bool $replace
         * @return void
         * @throws ReflectionException
         */
        public static function mixin($mixin, $replace = true)
        {
            QueryBuilder::mixin($mixin, $replace);
        }

        /**
         * Checks if macro is registered.
         *
         * @param string $name
         * @return bool
         */
        public static function hasMacro($name)
        {
            return QueryBuilder::hasMacro($name);
        }

        /**
         * Dynamically handle calls to the class.
         *
         * @param string $method
         * @param array $parameters
         * @return mixed
         * @throws BadMethodCallException
         */
        public static function macroCall($method, $parameters)
        {
            /** @var QueryBuilder $instance */
            return $instance->macroCall($method, $parameters);
        }
    }

    class FormBuilder extends App\Facades\FormBuilder
    {
    }

}
