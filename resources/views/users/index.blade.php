<?php
use App\User;
/** @var User[] $users */
?>
@extends('layouts.app')
@section('content')
    @can('approved')

    @endcan
    <div class="card">
        <div class="card-header">
            {{ trans('user.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        @can('admin')
                        <th width="10">

                        </th>
                        @endcan
                        <th>
                            {{ trans('user.fields.name') }}
                        </th>
                        <th>
                            {{ trans('user.fields.email') }}
                        </th>
                        <th>
                            {{ trans('user.fields.sex') }}
                        </th>
                        <th>
                            {{ trans('user.fields.old') }}
                        </th>
                        <th>
                            {{ trans('user.fields.status') }}
                        </th>
                        <th>
                            {{ trans('user.fields.role') }}
                        </th>
                        <th>
                            {{ trans('user.fields.invited_by_id') }}
                        </th>
                        @can('admin')
                        <th>
                            &nbsp;
                        </th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($users as $key => $user) { ?>
                        <tr data-entry-id="{{ $user->id }}">
                            @can('admin')
                            <td>
                                <input type="checkbox" name="user[{{ $user->id }}]"/>
                            </td>
                            @endcan
                            <td>
                                {{ $user->name }}
                            </td>
                            <td>
                                {{ $user->email }}
                            </td>
                            <td>
                                <?= User::SEX[$user->sex] ?>
                            </td>
                            <td>
                                {{ $user->old }}
                            </td>
                            <td>
                                <?= User::STATUSES[$user->status] ?>
                            </td>
                            <td>
                                <?= User::ROLES[$user->role] ?>
                            </td>
                            <td>
                                <?= $user->invitedBy->name ?>
                            </td>
                            @can('admin')
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.users.show', $user->id) }}">
                                    {{ trans('global.view') }}
                                </a>
                                <a class="btn btn-xs btn-info" href="{{ route('admin.users.edit', $user->id) }}">
                                    {{ trans('global.edit') }}
                                </a>
                                <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                            </td>
                            @endcan
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
{{--            @can('user_delete')--}}

{{--            @endcan--}}
        })

    </script>
@endsection