<?php

return [
    'title'          => 'Users',
    'title_singular' => 'User',
    'fields'         => [
        'id'                       => 'ID',
        'id_helper'                => '',
        'name'                     => 'Имя',
        'name_helper'              => '',
        'email'                    => 'Email',
        'email_helper'             => '',
        'sex'                      => 'Пол',
        'sex_helper'               => '',
        'orient'                   => 'Ориентация',
        'orient_helper'            => '',
        'direct'                   => 'Направление',
        'direct_helper'            => '',
        'old'                      => 'Возраст',
        'old_helper'               => '',
        'status'                   => 'Статус',
        'status_helper'            => '',
        'role'                     => 'Роль',
        'role_helper'              => '',
        'invited_by_id'            => 'Поручитель',
        'invited_by_id_helper'     => '',
        'email_verified_at'        => 'Email подтвержден',
        'email_verified_at_helper' => '',
        'roles'                    => 'Роли',
        'roles_helper'             => '',
        'created_at'               => 'Создан',
        'created_at_helper'        => '',
        'updated_at'               => 'Изменен',
        'updated_at_helper'        => '',
    ],
    'roles' => [
        'admin' => 'Админ',
        'user' => 'Участник',
        'viewer' => 'Наблюдатель',
        'disabled' => 'На отдыхе',
    ],
];
