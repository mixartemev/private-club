<?php

return [
    'title'          => 'Users',
    'title_singular' => 'User',
    'fields'         => [
        'id'                       => 'ID',
        'id_helper'                => '',
        'name'                     => 'Name',
        'name_helper'              => '',
        'email'                    => 'Email',
        'email_helper'             => '',
        'email_verified_at'        => 'Email verified at',
        'email_verified_at_helper' => '',
        'roles'                    => 'Roles',
        'roles_helper'             => '',
        'created_at'               => 'Created at',
        'created_at_helper'        => '',
        'updated_at'               => 'Updated at',
        'updated_at_helper'        => '',
    ],
];
