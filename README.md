# Private Club

## Installation
```bash
php composer install
cp .env.example .env
# configure your .env
php artisan key:generate
php artisan storage:link
php artisan migrate:fresh --seed
chmod -R 777 storage bootstrap/cache # for prod
```
