<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $sex
 * @property int $orient
 * @property int $direct
 * @property int $old
 * @property int $status
 * @property int $role
 * @property int $invited_by_id
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static Builder|User withTrashed()
 * @method static Builder|User withoutTrashed()
 * @mixin Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const STATUS_WAIT = 0;
    const STATUS_APPROVE = 1;
    const STATUS_EXP = 2;
    const STATUS_TRINITY = 3;
    const STATUS_GRAND = 4;
    const STATUS_BAN = -1;
    const STATUS_BLOCK = -2;

    const STATUSES = [
        self::STATUS_WAIT => 'waiting',
        self::STATUS_APPROVE => 'approved',
        self::STATUS_EXP => 'exp1',
        self::STATUS_TRINITY => 'trinity',
        self::STATUS_GRAND => 'grand10',
        self::STATUS_BAN => 'banned',
        self::STATUS_BLOCK => 'blocked',
    ];

    const ROLE_DISABLE = 0;
    const ROLE_VIEW = 1;
    const ROLE_USE = 2;
    const ROLE_ADMIN = 4;

    const ROLES = [
        self::ROLE_DISABLE => 'disabled',
        self::ROLE_VIEW => 'viewer',
        self::ROLE_USE => 'user',
        self::ROLE_ADMIN => 'admin',
    ];

    const SEX_MALE = 1;
    const SEX_FEMALE = 2;
    const SEX_SHEMALE = 3;

    const SEX = [
        self::SEX_MALE => 'boy',
        self::SEX_FEMALE => 'girl',
        self::SEX_SHEMALE => 'trans',
    ];

    const ORIENT_HETERO = 1;
    const ORIENT_HOMO = 2;
    const ORIENT_BI = 3;

    const ORIENTS = [
        self::ORIENT_HETERO => 'hetero',
        self::ORIENT_HOMO => 'homo',
        self::ORIENT_BI => 'bi',
    ];

    const DIRECTION_ACTIVE = 1;
    const DIRECTION_PASSIVE = 2;
    const DIRECTION_UNIVERSAL = 3;

    const DIRECTIONS = [
        self::DIRECTION_ACTIVE => 'active',
        self::DIRECTION_PASSIVE => 'passive',
        self::DIRECTION_UNIVERSAL => 'universal',
    ];

    /**
     * @return BelongsTo|User
     */
    public function invitedBy()
    {
        return $this->belongsTo(self::class);
    }

    /**
     * @return HasMany|Collection|User[]
     */
    public function invited()
    {
        return $this->hasMany(self::class, 'id', 'invited_by_id');
    }
}
