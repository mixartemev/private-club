<?php

namespace App\Services\Form;

class Html
{
    const FIELD_ATTRIBUTES = [
        'type',
        'name',
        'id',
        'class',
        'placeholder',
        'value',
        'step',
        'multiple',
    ];

    /**
     *
     * @var Form
     */
    protected $form;

    /**
     *
     * @param Form $form
     *
     * @return void
     */
    public function __construct(Form $form)
    {
        $this->form = $form;
    }

    /**
     *
     * @return string
     */
    public function draw(): string
    {
        $errors = $this->form->getErrors();

        list($method, $subMethod) = $this->form->resolveMethod();

        $id = $this->form->getId();

        $form = '<form id="' . $id . '" action="' . $this->form->action . '" method="' . $method . '" class="' .
            $this->form->formClass . '">';

        $html = csrf_field();
        if (!empty($subMethod)) {
            $html .= '<input type="hidden" name="_method" value="' . $subMethod . '">';
        }

        foreach ($this->form->fields as $field) {
            $errorClass = !empty($errors[$field['name']]) ? 'has-error' : '';
            $errorMsg = !empty($errors[$field['name']]) ? implode(', ', $errors[$field['name']]) : '';

            $html .= '<div class="js-group ' . $field['groupClass'] . ' ' . $errorClass . '">';
            $html .= $this->getHtmlLabel($field);
            $html .= $this->{$field['tag'] . ''}($field);

            if (!empty($field['hint'])) {
                $html .= '<span class="helper-block">' . $field['hint'] . '</span>';
            }

            $html .= '<span class="help-block">' . $errorMsg . '</span>';
            $html .= '</div>';
        }

        if (!empty($this->form->buttons)) {
            $html .= '<div>';

            foreach ($this->form->buttons as $button) {
                $html .= '<input class="btn ' . $button['class'] . '" type="' . $button['type'] . '" value="' .
                    $button['label'] . '">';
            }

            $html .= '</div>';
        }

        if (!empty($this->form->ajaxifyPanel)) {
            $html .= '<div class="' . ($this->form->ajaxifyPanel['options']['groupClass'] ?? '') . '" >';
            $html .= '<label class="w-100">&nbsp;</label>';

            if ($this->form->model->exists) {
                if (!empty($this->form->ajaxifyPanel['actions']['edit']) &&
                    $this->form->ajaxifyPanel['actions']['edit']['hasAccess']
                ) {
                    $html .= ' <button class="btn btn-info js-ajaxify-btn-edit" type="button">'
                        . '<i class="fas fa-edit"></i></button>';
                }
                if (!empty($this->form->ajaxifyPanel['actions']['destroy']) &&
                    $this->form->ajaxifyPanel['actions']['destroy']['hasAccess']
                ) {
                    $html .= ' <button class="btn btn-danger js-ajaxify-btn-destroy" type="button" data-url="' .
                        $this->form->ajaxifyPanel['actions']['destroy']['url']
                        . '"><i class="fas fa-trash-alt"></i></button>';
                }
            } else {
                $html .= ' <button class="btn btn-success js-ajaxify-btn-store" type="button">+</button>';
            }

            $html .= '</div>';
        }

        return $form . $this->addWrapper($html) . '</form>';
    }

    /**
     *
     * @param array $field
     *
     * @return string
     */
    public function getHtmlLabel(array $field): string
    {
        $class = $this->form->labelConfig[$field['name']]['class'] ?? '';

        return '<label for="' . $field['name'] . '" class="' . $class . '">' . $field['label'] . '</label>';
    }

    /**
     *
     * @param array $attributes
     *
     * @return string
     */
    public function input(array $attributes): string
    {
        return '<input ' . join(' ', array_map(function ($key) use ($attributes) {
                if (in_array($key, self::FIELD_ATTRIBUTES)) {
                    return $key . '="' . $attributes[$key] . '"';
                }
            }, array_keys($attributes))) . ' />';
    }

    /**
     *
     * @param array $attributes
     *
     * @return string
     */
    public function textarea(array $attributes): string
    {
        return '<textarea ' . join(' ', array_map(function ($key) use ($attributes) {
                if (in_array($key, self::FIELD_ATTRIBUTES) && $key !== 'value') {
                    return $key . '="' . $attributes[$key] . '"';
                }
            }, array_keys($attributes))) . '>' . $attributes['value'] . '</textarea>';
    }

    /**
     *
     * @param array $attributes
     *
     * @return string
     */
    public function dropzone(array $attributes): string
    {
        return '<div class="needsclick dropzone" id="' . $attributes['name'] . '-dropzone"></div>';
    }

    /**
     *
     * @param array $attributes
     *
     * @return string
     */
    public function select(array $attributes): string
    {
        if (!empty($attributes['multiple'])) {
            $attributes['class'] = isset($attributes['class']) ?
                $attributes['class'] . ' select2' : $attributes['class'];
        }

        $html = '<select ' . join(' ', array_map(function ($key) use ($attributes) {
                if (in_array($key, self::FIELD_ATTRIBUTES) && $key !== 'value') {
                    return $key . '="' . $attributes[$key] . '"';
                }
            }, array_keys($attributes))) . '>';

        if (!isset($attributes['empty']) || $attributes['empty'] !== false) {
            $empty = !empty($attributes['empty']) ? $attributes['empty'] : '-';
            $html .= '<option value="">' . $empty . '</option>';
        }

        foreach ($attributes['options'] as $k => $v) {
            $selected = (!is_array($attributes['value']) && $attributes['value'] == $k) ||
            (is_array($attributes['value']) && in_array($k, $attributes['value']))
                ? 'selected="selected"' : '';

            $html .= '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
        }

        $html .= '</select>';

        return $html;
    }

    /**
     *
     * @param array $attributes
     *
     * @return string
     */
    public function toggle(array $attributes): string
    {
        $html = '<div class="btn-group from_builder_buttons">';
        $html .= '<input type="hidden" id="' . $attributes['id'] . '" name="' . $attributes['name']
            . '" value="' . $attributes['value'] . '"/>';

        foreach ($attributes['options'] as $k => $v) {
            $class = $attributes['value'] == $k ? 'btn-info' : 'btn-default';
            $html .= '<button type="button" class="js-toggle-btn btn  ' . $class . '" data-value="' . $k . '">'
                . $v . '</button>';
        }

        $html .= '</div>';

        return $html;
    }

    /**
     *
     * @param string $html
     *
     * @return string
     */
    public function addWrapper(string $html): string
    {
        $htmlWithWrappers = '';

        if (!empty($this->form->wrappers)) {
            foreach ($this->form->wrappers as $wrapper) {
                $htmlWithWrappers .= '<div ' . join(' ', array_map(function ($key) use ($wrapper) {
                        return $key . '="' . $wrapper[$key] . '"';
                    }, array_keys($wrapper))) . '>';
            }

            $htmlWithWrappers .= $html;

            $htmlWithWrappers .= '</div>' * count($this->form->wrappers);
        } else {
            return $html;
        }

        return $htmlWithWrappers;
    }
}
