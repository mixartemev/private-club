<?php

namespace App\Services\Form;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\ViewErrorBag;
use ReflectionClass;

class Form
{
    /**
     *
     * @var Model
     */
    public $model;

    /**
     * The form id.
     *
     * @var string
     */
    protected $id;

    /**
     * The form method.
     *
     * @var string
     */
    public $method;

    /**
     * The form action.
     *
     * @var string
     */
    public $action;

    /**
     * The form class attribute.
     *
     * @var string
     */
    public $formClass = '';

    /**
     * The field wrap attribute.
     *
     * @var string
     */
    public $groupClass = 'form-group';

    /**
     * The field class.
     *
     * @var string
     */
    public $fieldClass = 'form-control';


    /**
     * wrappers.
     *
     * @var array
     */
    public $wrappers = [];

    /**
     * @var array
     */
    public $ajaxifyPanel = [];

    /**
     * @var array
     */
    public $fields = [];

    /**
     * @var array
     */
    public $buttons = [];

    /**
     * @var array
     */
    public $labelConfig = [];

    /**
     * @var array
     */
    public $valueConfig = [];

    /**
     * Create a new form.
     * @param array $params
     * @param Closure|null $callback
     * @return void
     */
    public function __construct(array $params, Closure $callback = null)
    {
        foreach ($params as $k => $v) {
            if (property_exists($this, $k)) {
                $this->$k = $v;
            }
        }

        if (!is_null($callback)) {
            $callback($this);
        }
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id ?? Str::random(32);
    }

    /**
     * @param string $name
     * @param array $options
     * @param array $params
     * @return self
     */
    public function select(string $name, array $options, array $params = []): self
    {
        $this->resolveFieldConfig($name, $params);

        $this->addField('select', [
                'name' => !empty($params['multiple']) ? "{$name}[]" : $name,
                'options' => $options,
            ] + $params);

        return $this;
    }

    /**
     * @param string $name
     * @param array $options
     * @param array $params
     * @return self
     */
    public function toggle(string $name, array $options = [], array $params = []): self
    {
        if (empty($params['value'])) {
            $params['value'] = 0;
        }

        $this->resolveFieldConfig($name, $params);

        if (empty($options)) {
            $options = [
                0 => __('global.no'),
                1 => __('global.yes'),
            ];
        }

        $this->addField('toggle', [
                'name' => $name,
                'options' => $options,
            ] + $params);

        return $this;
    }

    /**
     * @param string $type
     * @param string $name
     * @param array $options
     * @return self
     */
    public function input(string $type, string $name, array $options = []): self
    {
        $this->resolveFieldConfig($name, $options);

        $this->addField('input', [
                'type' => $type,
                'name' => $name,
            ] + $options);

        return $this;
    }

    /**
     * @param string $name
     * @param array $options
     * @return self
     */
    public function text(string $name, array $options = []): self
    {
        return $this->input('text', $name, $options);
    }

    /**
     * @param string $name
     * @param array $options
     * @return self
     */
    public function number(string $name, array $options = []): self
    {
        return $this->input('number', $name, $options);
    }

    /**
     * @param string $name
     * @param array $options
     * @return self
     */
    public function email(string $name, array $options = []): self
    {
        return $this->input('email', $name, $options);
    }

    /**
     * @param string $name
     * @param array $options
     * @return self
     */
    public function textarea(string $name, array $options = []): self
    {
        $this->resolveFieldConfig($name, $options);

        $this->addField('textarea', [
                'name' => $name,
            ] + $options);

        return $this;
    }

    /**
     * @param string $name
     * @param array $options
     * @return self
     */
    public function dropzone(string $name, array $options = []): self
    {
        $this->addField('dropzone', [
                'name' => $name,
            ] + $options);

        return $this;
    }

    /**
     * @param string $name
     * @return string
     */
    public function resolveName(string $name): string
    {
        $name = str_replace(['[', ']'], '', $name);

        return $name;
    }


    /**
     * @param string $name
     * @return string
     */
    public function label(string $name): string
    {
        $name = $this->resolveName($name);

        return $this->labelConfig[$name]['title'] ?? __('cruds.' . $this->getModelKey() . '.fields.' . $name);
    }

    /**
     * @param string $name
     * @return string
     */
    public function value(string $name): string
    {
        $name = $this->resolveName($name);

        if (array_key_exists($name, old())) {
            return old($name);
        }

        return $this->valueConfig[$name] ?? $this->model->{$name};
    }

    /**
     * @param string $name
     * @return string
     */
    public function hint(string $name): string
    {
        $name = $this->resolveName($name);

        return __('cruds.' . $this->getModelKey() . '.fields.' . $name . '_helper');
    }

    /**
     * Generate field attributes
     * @param string $tag
     * @param array $options
     * @return void
     */
    public function addField(string $tag, array $options = []): void
    {
        $options['tag'] = $tag;
        $options['id'] = $options['name'];
        $options['label'] = $this->label($options['name']);
        $options['hint'] = $this->hint($options['name']);

        if (!in_array($tag, ['dropzone'])) {
            $options['value'] = $this->value($options['name']);
        }

        if (!isset($options['groupClass'])) {
            $options['groupClass'] = $this->groupClass;
        }

        if (!isset($options['class'])) {
            $options['class'] = $this->fieldClass;
        }

        $this->fields[$options['name']] = $options;
    }

    /**
     * Generate button attributes
     * @param string $type
     * @param string $class
     * @param string $label
     * @return void
     */
    public function button(string $type, string $class, string $label): void
    {
        $this->buttons[] = [
            'type' => $type,
            'class' => $class,
            'label' => $label,
        ];
    }

    /**
     * Generate wrapper attributes
     * @param array $options
     * @return void
     */
    public function wrapper(array $options): void
    {
        $this->wrappers[] = $options;
    }

    /**
     * @param array $actions
     * @param array $options
     * @return void
     */
    public function ajaxifyPanel(array $actions, array $options): void
    {
        $this->ajaxifyPanel['actions'] = $actions;
        $this->ajaxifyPanel['options'] = $options;
    }

    /**
     * @return string
     */
    protected function getModelKey(): string
    {
        $shortName = (new ReflectionClass($this->model))->getShortName();

        return Str::camel($shortName);
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        $errors = [];
        if (session('errors') !== null && session('errors') instanceof ViewErrorBag) {
            $errors = session('errors')->getMessages();
        }

        return $errors;
    }

    /**
     * @return array
     */
    public function resolveMethod(): array
    {
        $method = strtoupper($this->method);
        $subMethod = '';
        if (in_array($method, ['PUT', 'DELETE'])) {
            $subMethod = $method;
            $method = 'POST';
        }

        return [$method, $subMethod];
    }

    /**
     * @param string $field
     * @param array $params
     * @return void
     */
    public function resolveFieldConfig(string $field, array $params): void
    {
        if (!empty($params['label'])) {
            $this->labelConfig[$field] = $params['label'];
        }

        if (isset($params['value'])) {
            $this->valueConfig[$field] = $params['value'];
        }
    }
}
